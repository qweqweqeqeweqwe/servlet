package jp.co.nttdata.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JspSample extends HttpServlet {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1751356779488685371L;

    public void init() throws ServletException {
        System.out.println("This is initMethod");
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        System.out.println("This is doGet");
        resp.setContentType("text/html;charset=UTF-8");
        String loginId = req.getParameter("loginId");
        String login = req.getParameter("login");
        req.setAttribute("loginId", loginId);
        req.setAttribute("loginName", "nttdata");
        req.setAttribute("login", login);
        req.getRequestDispatcher("/response.jsp").forward(req, resp);
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }

    public void destroy() {
        super.destroy();
        System.out.println("This is destroy");
    }
}
